<?php
namespace App\Core;

use App\Core\Decks\DeckGeneratorInterface;
use App\Core\Players\PlayersGeneratorInterface;

abstract class AbstractCardgame
{
    private $deckGenerator;
    private $playersGenerator;

    protected $outputStreamer;
    protected $game;
    protected $deck;
    protected $players= [];

    public function __construct(DeckGeneratorInterface $deckGenerator, PlayersGeneratorInterface $playersGenerator, $outputStreamer, $game)
    {
        $this->deckGenerator = $deckGenerator;
        $this->playersGenerator = $playersGenerator;
        $this->outputStreamer = $outputStreamer;
        $this->game = $game;
    }

    protected function setDeck()
    {
        $this->deck = $this->deckGenerator->getdeck();
    }

    protected function setplayers()
    {
        $this->players = $this->playersGenerator->getplayers($this->deck);
    }

    public function play()
    {
        $this->setDeck();
        $this->setplayers();
        $this->outputStreamer->display(" Begin " . $this->game);
        $this->runGame();
    }
}
