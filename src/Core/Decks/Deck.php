<?php
namespace App\Core\Decks;

class Deck
{
    private $cards = [];

    public function __construct(array $cards)
    {
        $this->cards = $cards;
    }

    public function getCards(): array
    {
        return $this->cards;
    }

    public function shuffle(): self
    {
        shuffle($this->cards);

        return $this;
    }

    public function playFirst() {
        return array_shift($this->cards);
    }

    public function addCards(array $cards)
    {
        foreach ($cards as $card) {
            $this->cards[] =  $card;
        }
    }
}
