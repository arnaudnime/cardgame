<?php
namespace App\Core\Decks;

interface DeckGeneratorInterface
{
    public function getDeck():Deck;
}
