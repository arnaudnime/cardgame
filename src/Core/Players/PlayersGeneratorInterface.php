<?php
namespace App\Core\Players;

use App\Core\Decks\Deck;

Interface PlayersGeneratorInterface
{
    public function getPlayers(Deck $deck): array;
}
