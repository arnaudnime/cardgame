<?php
namespace App\Core\Players\Factories\SimplePlayer;

use App\Core\Players\Factories\PlayerFactoryinterface;

class SimplePlayerFactory implements PlayerFactoryinterface
{
    public function create($name, $deck): SimplePlayer
    {
        return new SimplePlayer($name, $deck);
    }
}
