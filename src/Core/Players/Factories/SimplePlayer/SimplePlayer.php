<?php
namespace App\Core\Players\Factories\SimplePlayer;

use App\Core\Decks\Deck;

class SimplePlayer
{
    private $name;
    /** @var Deck $deck*/
    private $deck;

    public function __construct(string $name, Deck $deck)
    {
        $this->name = $name;
        $this->deck = $deck;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDeck(): Deck
    {
        return $this->deck;
    }

    public function noMoreCards()
    {
        return empty($this->deck->getCards());
    }

    public function getDeckCount()
    {
        return count($this->deck->getCards());
    }
}
