<?php
namespace App\Core\Players\Factories;

interface PlayerFactoryinterface
{
    public function create($name, $deck);
}
