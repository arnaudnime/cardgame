<?php
namespace App\Games;

interface GameInterface
{
    public function runGame();
}
