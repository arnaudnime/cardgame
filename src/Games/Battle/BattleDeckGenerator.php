<?php
namespace App\Games\Battle;

use App\Core\Decks\Deck;
use App\Core\Decks\DeckGeneratorInterface;

class BattleDeckGenerator implements DeckGeneratorInterface
{
    private $deckGameCount;

    public function __construct(int $deckGameCount)
    {
        $this->deckGameCount = $deckGameCount;
    }

    public function getDeck(): Deck
    {
        $cards = [];
        for ($i = 1; $i <= $this->deckGameCount; $i++) {
            $cards[] = $i;
        }

        return (new Deck($cards))->shuffle();
    }
}
