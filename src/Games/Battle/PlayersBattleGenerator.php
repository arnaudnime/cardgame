<?php
namespace App\Games\Battle;

use App\Core\Decks\Deck;
use App\Core\Players\Factories\SimplePlayer\SimplePlayerFactory;
use App\Core\Players\PlayersGeneratorInterface;

class PlayersBattleGenerator implements PlayersGeneratorInterface
{
    private $playersCount;
    private $playerDeckCount;
    private $playerFactory;

    public function __construct($playersCount, $playerDeckCount)
    {
        $this->playersCount = $playersCount;
        $this->playerDeckCount = $playerDeckCount;
        $this->playerFactory = new SimplePlayerFactory();
    }

    public function getplayers(Deck $gameDeck): array
    {
        $players = [];
        for ($i = 1; $i <= $this->playersCount; $i++) {
            $players[] = $this->playerFactory->create('player' . $i, $this->getDeckPlayer($i, $gameDeck));
        }

        return $players;
    }

    private function getDeckPlayer(int $number, Deck $gameDeck): Deck
    {
        $offset = (1 === $number) ? 0 : $this->playerDeckCount;
        $playerCards = array_slice($gameDeck->getCards(),$offset, $this->playerDeckCount);

        return new Deck($playerCards);
    }
}
