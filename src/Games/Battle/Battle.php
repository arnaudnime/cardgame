<?php
namespace App\Games\Battle;

use App\Core\AbstractCardgame;
use App\Output\ConsoleLogger;
use App\Games\GameInterface;
use App\Core\Players\Factories\SimplePlayer\SimplePlayer;

class Battle extends AbstractCardgame implements GameInterface
{
    const PLAYERS_COUNT = 2;
    const PLAYER_DECK_COUNT = 26;
    const DECK_GAME_COUNT = 52;
    const GAME_NAME = 'Battle card';

    /** @var SimplePlayer $player1*/
    private $player1;
    /** @var SimplePlayer $player2*/
    private $player2;
    /** @var SimplePlayer $winner*/
    private $winner;
    private $turnsCount = 0;
    private $withStream = false;

    public function __construct($options)
    {
        if (self::PLAYERS_COUNT * self::PLAYER_DECK_COUNT > self::DECK_GAME_COUNT) {
            echo("\n ... configuration error ...");
            die;
        }

        $this->setOptions($options);
        $deckGameGenerator = new BattleDeckGenerator(self::DECK_GAME_COUNT);
        $playersGenerator = new PlayersBattleGenerator(self::PLAYERS_COUNT, self::PLAYER_DECK_COUNT);
        $outputStreamer = new ConsoleLogger();

        parent::__construct(
            $deckGameGenerator,
            $playersGenerator,
            $outputStreamer,
            self::GAME_NAME
        );
    }

    public function runGame()
    {
        $this->player1 = $this->players[0];
        $this->player2 = $this->players[1];

        while (!$this->endGame()) {
            $this->applyTurn();
        }

        $this->displayWinner();
    }

    private function setoptions($options)
    {
        if (isset($options[1]) && '-v' === $options[1]) {
            $this->withStream = true;
        }
    }

    private function applyTurn()
    {
        $this->turnsCount ++;
        $cardPlayer1 = $this->player1->getDeck()->playFirst();
        $cardPlayer2 = $this->player2->getDeck()->playFirst();

        if ($cardPlayer1 > $cardPlayer2) {
            $this->player1->getDeck()->addCards([$cardPlayer1, $cardPlayer2]);
            if ($this->withStream) {
                $this->displayStream("$cardPlayer1 > $cardPlayer2");
            }
        } else {
            $this->player2->getDeck()->addCards([$cardPlayer1, $cardPlayer2]);
            if ($this->withStream) {
                $this->displayStream("$cardPlayer1 < $cardPlayer2");
            }
        }
    }

    private function endGame()
    {
        if ($this->player1->noMoreCards()) {
            $this->winner = $this->player2;
        }
        if ($this->player2->noMoreCards()) {
            $this->winner = $this->player1;
        }

        return !empty($this->winner);
    }

    function displayStream(string $result): void
    {
        $player1Result =  ' Player 1 (' . $this->player1->getDeckCount() . ')';
        $player2Result =  ' Player 2 (' . $this->player2->getDeckCount() . ')';
        $this->outputStreamer->display("\033[31m".$result."\033[00m" . $player1Result . $player2Result);
    }

    private function displayWinner()
    {
        $this->outputStreamer->emptyLine();
        $this->outputStreamer->display(' !!! VICTORY !!!');
        $this->outputStreamer->display(' !!! ' . $this->winner->getName() . ' !!!');
        $this->outputStreamer->emptyLine();
    }
}
