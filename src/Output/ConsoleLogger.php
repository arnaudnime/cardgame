<?php
namespace App\Output;

class ConsoleLogger implements OutputInterface
{
    public function emptyLine() :void
    {
        echo "\n";
    }

    public function display(string $content)
    {
        echo "\n" . $content;
    }

    public function debug($log) :void
    {
        var_dump($log);
    }
}
