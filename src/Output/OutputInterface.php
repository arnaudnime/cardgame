<?php
namespace App\Output;

interface OutputInterface
{
    public function display(string $content);
}
